FROM python:3.7

RUN mkdir /event-poster && \
    pip install meetup-api

WORKDIR /event-poster

COPY post.py .

CMD /event-poster/post.py
