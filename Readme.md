# Meetup Upcoming Events

---

Just fill in the blanks on the `event.json`, then follow the instructions below.

---

[Get your API key here](https://secure.meetup.com/meetup_api/key/)

This must be saved locally as `MEETUP_API_KEY`

`export MEETUP_API_KEY="<your_meetup_api_key"`

---

### Using Python 3 locally

```
pip install meetup-api
./post.py
```

---

### Using Docker

```
docker build -t event-poster .
docker run -e "MEETUP_API_KEY=$MEETUP_API_KEY" -v $(pwd)/event.json:/event-poster/event.json event-poster
```

---

### Required fields in the event.json (with SFS defaults)

_Note: This script is based on the Python API client, which uses v2 of the events API._

[Full event creation documentation at Meetup](https://www.meetup.com/meetup_api/docs/2/event/#create)

* name - Title of your event, string
* group_id - 19198932 (integer output of `curl --silent https://api.meetup.com/sofreeus | jq '.id'`)
* group_url_name - sofreeus
* publish_status - draft; omit this to publish
* announce - false; whether or not to announce event to users
* hosts - "1826027"; <<< That's DL Willson.  Look up users via URL at meetup.com.  Can have up to 5, comma-separated.
* featured_photo_id -  476322827; Tricky to find.  I used Chrome dev tools `inspect`.
* duration - 14400000; event duration in ms ... in case you want to have a _really_ short event
* rsvp_limit - 16
* time - 1546704000000; Use something like [this](https://mainfacts.com/timestamp-date-converter-calculator), then multiply by 1000.  Because reasons.
* venue_id - 24250060; This is the comfy room.  Manually create an event, then use the API to [query that event](https://www.meetup.com/meetup_api/docs/:urlname/events/:id/#get) and scrape the venue id.
* description - Check the documentation (linked above) this allows a subset of HTML.
* visibility - public
* fee.accepts - cash
* fee.amount - 16
* fee.currency - USD
