#!/usr/bin/env python

import meetup.api, json

client = meetup.api.Client()

with open('event.json',r) as f:
    event_data = f.read()

event_data_json = json.loads(event_data)

response = client.CreateEvent(**event_data_json)

resp_dict = response.__dict__

print(resp_dict)
